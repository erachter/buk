from django.shortcuts import render, redirect
from orders.models import Order

def receive_order(request):
    if request.method == 'POST':
        print(request.POST)
        order = Order()
        order.kid_name = request.POST.get("name", "")
        order.age = request.POST.get("age", 8)
        order.gender = request.POST.get("gender", 0)
        order.customer_name = request.POST.get("customer_name", "")
        order.customer_email = request.POST.get("customer_email", "")
        order.customer_phone = request.POST.get("customer_phone", "")
        order.save()
        return redirect('/thank-you/')

    return redirect('/')

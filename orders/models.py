from django.db import models
import django

GENDERS = (
    (0, 'MALE'),
    (1, 'FEMALE')
)

class Order(models.Model):
    kid_name = models.CharField(max_length=128, blank=True)
    gender = models.PositiveIntegerField(choices=GENDERS, default=0)
    age = models.IntegerField(default=10)
    date = models.DateTimeField(default=django.utils.timezone.now)

    customer_name = models.CharField(max_length=128, blank=True)
    customer_email = models.CharField(max_length=128, blank=True)
    customer_phone = models.CharField(max_length=50, blank=True)
